import pdb

import datetime
import re
import json
import sys

from bottle import Bottle, static_file
import pandas as pd

sys.path.append('../')
from utils.cors import enable_cors
from utils.utils import query_df, read_config

app = Bottle()
app.install(enable_cors)


def list_filter(config):
    ''' Matches a comma separated list of numbers. '''
    delimiter = ','
    regexp = r'\d+(%s\d)*' % re.escape(delimiter)

    def to_python(match):
        return map(int, match.split(delimiter))

    def to_url(numbers):
        return delimiter.join(map(str, numbers))

    return regexp, to_python, to_url

app.router.add_filter('list', list_filter)

barPlot_route = '/barPlot_route/<index:path:list>'
dataIncidents_route = '/dataIncidents/<ligne:int>'
data = pd.read_csv("data/export_incidents.csv")
data["index"] = data.index
data["vitesse"] = data["vitesse"].str.split(" ", expand=True)[0]
data["vitesse"] = data["vitesse"].astype(int)
#data = query_df(data, ligne=405000)

data_extract = data.loc[:10000, ["pk", "vitesse", "date", "index", "ligne"]]


@app.route(dataIncidents_route)
def dataIncidents(ligne):
#    pdb.set_trace()
    data_query = query_df(data_extract, ligne=ligne)
#    pdb.set_trace()
    return data_query.to_json(orient="records")


@app.route(barPlot_route)
def barPlot_graph(index):
    index = [int(element) for element in index.split(",")]

    dataRequest = data_extract[data_extract["index"].isin(index)]
    dataRequest["vitesse_bins"] = pd.cut(dataRequest["vitesse"], bins=9)

    barPlotData = dataRequest.vitesse_bins.value_counts().rename(
        "count").reset_index().to_json(orient="records")

    return barPlotData

app.run(host="localhost", port="8080", debug=True)
