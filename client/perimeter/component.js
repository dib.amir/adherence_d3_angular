angular.
    module('perimeter', []).
    component('perimeter', {
	templateUrl: 'perimeter/template.html',
	controller: function($scope, StateService) {
	    $scope.data = {
		perimeter:[
		    {id : 'Argentan–Granville', 'ligne' :'405000'},
		    {id : 'Mans-Mézidon', 'ligne' : '430000'},
		    {id : 'Saint-Cyr-Surdon', 'ligne' : '395000'},
		    {id : 'Malaunay-Dieppe', 'ligne' : '350000'},
		    {id : 'Rouen–Le Havre', 'ligne' : '340000'}]
	    };
	    
	    $scope.selected = $scope.data.perimeter[0];
	    
	    $scope.changePerim = function changePerim(ligne) {
		StateService.setLigne(ligne);
	    };
	}
    });
	      
