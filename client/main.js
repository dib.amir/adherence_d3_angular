d3.json("data/data_extract.json", function(error, data){

/*--------------------------------------------------------------------------------------------- */
/*----------------------- Data FORMATTING------------------------------------------------------ */
/*--------------------------------------------------------------------------------------------- */
    var pointRadius = 2;
    
    var fullWidth = 1000;
    var fullHeight = 500;
    var margin = {top: 30, right: 10, bottom: 30, left: 50};
    var width = fullWidth - margin.left - margin.right;
    var height = fullHeight - margin.top - margin.bottom;
    
    var selectedPoint;

    data.forEach(function (el, i){
	el.xdate = new Date(el.date);
	el.selected = false;
	el.brushed = false;
	el.i = el.index;
    });

  var yExtent = d3.extent(data, function(el) {
	          return el.pk;
                });    
  var xExtent = d3.extent(data, function(el) {
	           return el.xdate;
	         });

  var xScale = d3.scaleTime().domain(xExtent).range([0,width]);
    var yScale = d3.scaleLinear().domain(yExtent).range([0,height]);

    data.forEach(function (el){
	el.x = xScale(el.xdate);
	el.y = yScale(el.pk);
    });

  
  var tree = d3.quadtree()
	     .x(function(d) {return xScale(d.xdate);})
	     .y(function(d) {return yScale(d.pk);})
	     .addAll(data);

  var canvas = d3.select("#plot-canvas")
               .attr("width", width - 1)
               .attr("height", height - 1)
               .style("transform", "translate(" + (margin.left + 1) +
                      "px" + "," + (margin.top + 1) + "px" + ")");
    var incident_plot = d3.select("#incident-plot");

    var brushsvg = d3.select("#brush-svg")
	.attr("width", width - 1)
	.attr("height", height - 1)
        .style("transform", "translate(" + (margin.left + 1) +
               "px" + "," + (margin.top + 1) + "px" + ")");
    

  var context = canvas.node().getContext('2d');

/*------------------------------------------------*/
/*-----------------------AXIS---------------------*/
/*------------------------------------------------*/
  var svg = d3.select("#axis-svg")
            .attr("width", fullWidth)
            .attr("height", fullHeight)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," +
                  margin.top + ")");

  var xAxis = d3.axisBottom(xScale)
              .tickSizeInner(-height)
              .tickSizeOuter(0);

  var yAxis = d3.axisLeft(yScale)
              .tickSizeInner(-width)
              .tickSizeOuter(0);

  var zoomBehaviour = d3.zoom()
                      .scaleExtent([1/2, 4])
                      .on("zoom", zoomed);
  
  var xAxisSvg = svg.append('g')
                 .attr('class', 'x axis')
                 .attr('transform', 'translate(0,' + height + ')')
                 .call(xAxis);
  
  var yAxisSvg = svg.append('g')
                 .attr('class', 'y axis')
                 .call(yAxis);
  
/*--------------------------------------------------*/
/*-----------------------EVENTS---------------------*/
/*--------------------------------------------------*/
    var brushsvg = d3.select("#brush-svg");

    var brush = d3.brush()
	    .on("start brush", brushed)
            .on("end", brushended);
  
    function brushed() {
	
	var extent = d3.event.selection;
	data.forEach(function(d) { d.brushed = false; });
	search(tree, extent[0][0], extent[0][1], extent[1][0], extent[1][1]);
    }


    
    function search(quadtree, x0, y0, x3, y3) {
	quadtree.visit(function(node, x1, y1, x2, y2) {
	    if (!node.length) {
		do {
		    var d = node.data;
		    d.brushed = (d.x >= (x0-pointRadius)) && (d.x < (x3+pointRadius)) && (d.y >= (y0-pointRadius)) && (d.y < (y3+pointRadius));
		} while (node = node.next);
	    }
	    return x1 >= x3 || y1 >= y3 || x2 < x0 || y2 < y0;
	});

	draw();
    }
  
//	incident_plot.on("mousemove", mouseMoved);

  draw();
  
  brushsvg.append("g")
  .attr("class", "brush")
  .call(brush)
  .call(brush.move, [[0, 0], [200, 200]]);
  
  function mouseMoved() {
    var mouse = d3.mouse(this);
    var scale_zoom = d3.zoomTransform(this).k
    var x_pan_offset = d3.zoomTransform(this).x
    var y_pan_offset = d3.zoomTransform(this).y
    var mouse_x = (mouse[0] - x_pan_offset)/scale_zoom ;
    var mouse_y = (mouse[1] - y_pan_offset)/scale_zoom ;
    
    var closest = tree.find(mouse_x, mouse_y);
    
    if(selectedPoint) {
      data[selectedPoint].selected = false;
    }
    closest.selected = true;
    selectedPoint = closest.i;
    
    context.save();
    context.clearRect(0, 0, width, height);
    context.translate(d3.zoomTransform(this).x,d3.zoomTransform(this).y);
    context.scale(d3.zoomTransform(this).k,d3.zoomTransform(this).k);
    draw();
    context.restore();    
  }
  
  function zoomed() {
    context.save();
    context.clearRect(0, 0, width, height);

    context.translate(d3.event.transform.x, d3.event.transform.y);
    context.scale(d3.event.transform.k, d3.event.transform.k);

    draw();
    context.restore();
  }

/*-------------------------------------------------------------*/
/*-----------------------DRAWING FUNCTIONS---------------------*/
/*-------------------------------------------------------------*/

  function draw(index) {
    var active;

    context.clearRect(0, 0, fullWidth, fullHeight);
    context.fillStyle = 'steelblue';
    context.strokeWidth = 1;
    context.strokeStyle = 'white';

    data.forEach(function(point) {
             if(!point.selected & !point.brushed) {
               drawPoint(point, pointRadius);
             }
             else {
               active = point;
             }
           });

      data.forEach(function(point){
	  if(point.brushed){
	      context.fillStyle='red';
	      drawPoint(point, pointRadius);
	      context.fillStyle = 'steelblue';	      
	  }
      });
    if(active) {
      context.fillStyle = 'red';
      drawPoint(active, pointRadius);
      context.fillStyle = 'steelblue';
    }
  }
  
  function drawPoint(point, r) {
    var cx = xScale(point.xdate);
    var cy = yScale(point.pk);
    
    context.beginPath();
    context.arc(cx, cy, r, 0, 2 * Math.PI);
    context.closePath();
    context.fill();
    context.stroke();
  }


/*-----------------------------------------------------------------------*/
/*-----------------------------BARPLOT-----------------------------------*/
/*-----------------------------------------------------------------------*/

var svg = d3.select("#bar-plot"),
    margin = {top: 20, right: 80, bottom: 30, left: 50},
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom;

var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
    y = d3.scaleLinear().rangeRound([height, 0]);

var g = svg.append("g")
	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    g.append("g")
	.attr("class", "axis--x")
	.attr("transform", "translate(0," + height + ")");
	
    g.append("g")
	.attr("class", "axis--y");
	


    function brushended() {
	
	brushedData_index = brushedData_index_array(data);

	request_data = "http://localhost:8000/barPlot_route/" + brushedData_index.join(",")
	console.log(request_data);    
	d3.json(request_data, function(barPlot_data){
//	    console.log(barPlot_data);
	    
	    if (error) throw error;

	    x.domain(barPlot_data.map(function(d) {   //console.log(d.index);
						      return d.index;
						  }));
	    y.domain([0, d3.max(barPlot_data, function(d) { //console.log(d.count);
							    return d.count;
							  })]);

	    d3.select(".axis--x")
		.call(d3.axisBottom(x));

	    d3.select(".axis--y")
		.call(d3.axisLeft(y).ticks(10, ""))
		.append("text")
		.attr("transform", "rotate(-90)")
		.attr("y", 6)
		.attr("dy", "0.71em")
		.attr("text-anchor", "end")
		.text("Frequency");

	    console.log(barPlot_data);
	    var bar = g.selectAll(".bar").data(barPlot_data, function(d){return d;});
	    
	    bar.enter()
		.append("rect")
		.attr("class", "bar")
		.attr("x", function(d) {
		    console.log(x(d.index));
		    return x(d.index); })
		.attr("y", function(d) { return y(d.count); })
		.attr("width", x.bandwidth())
		.attr("height", function(d) { return height - y(d.count); });

	    
		bar.exit().remove();
	});
	
	function brushedData_index_array(data){
	    var brushed = [];
	    data.forEach(function(e){ if (e.brushed) brushed.push(e.i);});
	    return brushed;
	};
    }
});
