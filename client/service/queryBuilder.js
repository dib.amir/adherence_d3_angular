angular
    .module('queryBuilder', [])
    .factory('QueryService', function() {
	return {
	    incidentsQuery: incidentsQuery
	};

 	function incidentsQuery(ligne) {
	    var queryUrl = 'dataIncidents/' + ligne ;
	    return buildQuery(queryUrl);
	};
	
 	function brushQuery(index) {
	    var queryUrl = 'dataIncidents/' + index ;
	    return buildQuery(queryUrl);
	};

	function buildQuery(queryUrl, config) {
	    var baseUrl = 'http://localhost:8080/';
	    return baseUrl + queryUrl;
	};	
    });
